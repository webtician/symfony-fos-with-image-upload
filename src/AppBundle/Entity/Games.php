<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Games
 *
 * @ORM\Table(name="games")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GamesRepository")
 */
class Games
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="stripped_name", type="string", length=50, unique=true)
     */
    private $strippedName;

    /**
     * @var string
     *
     * @ORM\Column(name="game_description", type="string", length=255)
     */
    private $gameDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=64)
     */
    private $image;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Games
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set strippedName
     *
     * @param string $strippedName
     *
     * @return Games
     */
    public function setStrippedName($strippedName)
    {
        $this->strippedName = $strippedName;

        return $this;
    }

    /**
     * Get strippedName
     *
     * @return string
     */
    public function getStrippedName()
    {
        return $this->strippedName;
    }

    /**
     * Set gameDescription
     *
     * @param string $gameDescription
     *
     * @return Games
     */
    public function setGameDescription($gameDescription)
    {
        $this->gameDescription = $gameDescription;

        return $this;
    }

    /**
     * Get gameDescription
     *
     * @return string
     */
    public function getGameDescription()
    {
        return $this->gameDescription;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Games
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}

