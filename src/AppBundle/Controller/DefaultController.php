<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('layout/layout.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }
    
    /**
     * @Route("/status/{status}")
     */
    public function showAction($status)
    {
       
        $userManager = $this->get('fos_user.user_manager');
        $usr= $this->get('security.context')->getToken()->getUser();
        $usr->getUsername();
        $user = $userManager->findUserByUsername($usr->getUsername());
        $user->setStatus($status);
        $userManager->updateUser($user);

        
        echo "status set too" . $status;

       // replace this example code with whatever you need
        return $this->render('layout/status.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }
}
