<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameServers
 *
 * @ORM\Table(name="game_servers")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameServersRepository")
 */
class GameServers
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="stripped_name", type="string", length=50, unique=true)
     */
    private $strippedName;

    /**
     * @var int
     *
     * @ORM\Column(name="game_id", type="integer")
     */
    private $gameId;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=64)
     */
    private $image;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GameServers
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set strippedName
     *
     * @param string $strippedName
     *
     * @return GameServers
     */
    public function setStrippedName($strippedName)
    {
        $this->strippedName = $strippedName;

        return $this;
    }

    /**
     * Get strippedName
     *
     * @return string
     */
    public function getStrippedName()
    {
        return $this->strippedName;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     *
     * @return GameServers
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return int
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return GameServers
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}

